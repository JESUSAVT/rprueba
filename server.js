require('dotenv').config()
const express = require('express');//import
const body_parser = require('body-parser') //require =import. la linea define de constante para  cuando hacemos peticiones
const app=express(); //app es un objeto que contiene la funcionalidad express
const port=process.env.PORT||3000;
const URL_BASE='/techu/v1/';
const usersFile=require('./usuarios.json');

app.listen(port, function(){
  console.log('Nose JS escuchando en el puerto '+ port);
});
app.use(body_parser.json());

//operacion GET (collection)
app.get(URL_BASE + 'Rusers', //nombreRecusoEnPlural - .get= peticion, es una funcion que recibe 2 parametros
function(request,response) { //callback es una funcion anonima que  pasa como parametro de otra funcion
                            //cuando reciba la respuesta ejecuta la linea de abajo
  response.status(200);     // estas dos lines pude ser asi --> response.status(200).send(usersFile)
  response.send(usersFile); // la respuesta (send)al final -
});
//---------------------------*********----------------------------------------//
//Peticion GET  a  unico usuarios mediante ID(instancia)
app.get(URL_BASE + 'Rusers/:v_id_user',// /:id_user es una variable no es el nombre del campo
function (request,response){

  console.log(request.params.v_id_user);//
  let pos = request.params.v_id_user - 1; //obtener la posicion del array de usuarios que tenemos que enviar
                                        //que nos pasa en la URI que te devuelve la posicion
  let respuesta = (usersFile[pos]==undefined)? {"msg":"usuario no existe"}: usersFile[pos];// si el elemento en la posision que hemos enviado no existe
                                                                                          //envia msg, Een caso contrario devuelve el elemneto de la posision
                                                                                         //let variable = (condicion a==b)? "es_verdad":"es_falso"
  let status_code=(usersFile[pos]==undefined)?404:200;
  response.status(status_code).send(respuesta);
  //response.send(usersFile[pos]);// userFile= es una variable donde guarda toda el array
});
//---------------------------*********----------------------------------------//
//peticion POST a users
app.post(URL_BASE+'Rusers',
function(req,res){  //callback
  console.log('POST a REPOSITORIO users');
  let tam=usersFile.length; // tam = guardar el numero de elementos del array
  let new_user={ // estructura del nuevo usuario
    "id_user":tam + 1, // suma la cantidad de elemntos + 1 y lo pone en el nombre del campo
    "first_name":req.body.first_name, // los nombres de campos deben ser identicos
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  }
  console.log(new_user);
  usersFile.push(new_user); //insertamos al final ese nuevo usuario
  res.send({"msg": "Usuarios Creado correctamente"});
});
//---------------------------*********----------------------------------------//
//Peticion Put a Usuarios
app.put(URL_BASE + 'Rusers/:v_id',
   function(req, res){
     console.log("PUT /techu/v1/Rusers/:v_id");
     let idBuscar = req.params.v_id;
     let updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id_user);
       if(usersFile[i].id_user == idBuscar) {
         usersFile[i] = updateUser;
         res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
       }
     }
     res.send({"msg" : "Usuario no encontrado.", updateUser});
   });
//---------------------------*********----------------------------------------//
//peticion delete
app.delete(URL_BASE + 'Rusers/:v_id',
   function(req, res){
     console.log("DELETE /techu/v1/Rusers/:v_id");
     let idBuscar = req.params.v_id;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id_user);
       if(usersFile[i].id_user == idBuscar) {
         usersFile.splice(i,1)
         res.send({"msg" : "Usuario eliminado correctamente."});
       }
     }
     res.send({"msg" : "Usuario no encontrado eliminar."});
   });
//---------------------------*********----------------------------------------//
//PETICION GET CON QUERY STRING
app.get(URL_BASE + 'R2users',
function(req,res){
  console.log(req.query.id);
  console.log(req.query.country);
  //res.send(usersFile[pos - 1]);
  res.send({"msg":" GET CON QUERY"})
});
//---------------------------*********----------------------------------------//
// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST "+ URL_BASE + 'login');
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "login correcto.", "idUsuario" : us.id_user})
          //response.send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
        } else {
          console.log("Login incorrecto. mario");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
});
//---------------------------*********----------------------------------------//
function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   }); }
//---------------------------*********----------------------------------------//
   // LOGOUT - users.json
   // app.post(URL_BASE + 'logout1',
   //   function(request, response) {
   //     console.log("POST " + URL_BASE + 'logout');
   //     var userId = request.body.id_user;
   //     for(us of usersFile) {
   //       if(us.id_user == userId) {
   //         if(us.logged) {
   //           delete us.logged; // borramos propiedad 'logged'
   //           writeUserDataToFile(usersFile);
   //           console.log("Logout correcto!");
   //           response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
   //         } else {
   //           console.log("Logout incorrecto.");
   //           response.send({"msg" : "Logout incorrecto."});
   //         }
   //       }  us.logged = true
   //     }
   // });

   app.post(URL_BASE + 'logout/:v_userid',
   function(request, response) {
     let userId = request.params.v_userid;
     console.log("POST " + URL_BASE + 'logout/'+ userId);
     for (i=0; i< usersFile.length;i++){
       console.log("ayuda " + usersFile[i].id_user + " Variable "+ userId) ;
     //for(us of usersFile) {
       if(usersFile[i].id_user == userId) {
       //if(us.id_user == userId) {
         if(usersFile[i].logged) {
           delete usersFile[i].logged; // borramos propiedad 'logged'
           writeUserDataToFile(usersFile);
           console.log("Logout correcto!");
           response.send({"msg" : "Logout correcto.", "idUsuario" : usersFile[i].id_user});
           break;
           }
           else {
             //} else if (condition2) {
             console.log("Logout incorrecto.");
             response.send({"msg" : "Logout Incorrecto. - Usuario no Logueado", "idUsuario" : usersFile[i].id_user});
             break;
                }
                }
             }
             if (usersFile.length==i){
               console.log("Se alacanzo el recorrido total- no encontrado.");
               //response.send({"msg" : "Logout Incorrecto. - maestro"});
               response.status(404).send("Se alacanzo el recorrido total - "+userId+ " - no encontrado.");
             }
             });
app.get(URL_BASE + 'total_users',function(req,res){
  let tam=usersFile.length;
  var v_logueado=0;
  var v_unloggin=0;
  for(us of usersFile){
    if(us.logged){
      v_logueado = v_logueado + 1;
    }
    else{
      v_unloggin = v_unloggin + 1;
    }
    }
    console.log("num_usuarios " + tam);
    res.send({"num_usuarios ":tam,"logueados ":v_logueado,"no_logueados ":v_unloggin});
    });




//----------original--------------------//
     // app.post(URL_BASE + 'logout/:v_userid',
     // function(request, response) {
     //   let userId = request.params.v_userid;
     //   console.log("POST " + URL_BASE + 'logout/'+ userId);
     //   for(us of usersFile) {
     //     if(us.id_user == userId) {
     //       if(us.logged) {
     //         delete us.logged; // borramos propiedad 'logged'
     //         writeUserDataToFile(usersFile);
     //         console.log("Logout correcto!");
     //         response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
     //         }
     //         else {
     //           console.log("Logout incorrecto.");
     //           response.send({"msg" : "Logout incorrecto."});
     //              }
     //              }//  us.logged = true
     //           }
     //           });
//----------------**********************-----------------//


/////////////////obtener peticiones en variables//////////////////////
// //Peticion GET  a  unico usuarios mediante ID(instancia)
// app.get(URL_BASE + 'Rusers/:id_user/:p1/:p2',
// function (request,response){
//   console.log('v_id_user:' + request.params.id_user);
//   console.log('v_p1:' +request.params.p1);
//   console.log('v_p2:' +request.params.p2);
//   let pos = request.params.id_user - 1;
//   response.send(usersFile[pos]);
// })
