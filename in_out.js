require('dotenv').config()
const express = require('express'); //import
const body_parser = require('body-parser'); //require =import. la linea define de constante para  cuando hacemos peticiones
const request_json = require('request-json');
const app = express(); //app es un objeto que contiene la funcionalidad express
const port = process.env.PORT || 3000; //process.env llama al archivo .env y busca la variables
const URL_BASE = '/techu/v3/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu11db/collections/';
const apikeyMLab = 'apiKey=' + process.env.API_KEY_MLAB;
var httpClient = request_json.createClient(URL_mLab);

app.listen(port, function() {
  console.log('node utilizando el puerto ' + port );
});
app.use(body_parser.json());

/****************************
Method POST login
****************************/

app.post(URL_BASE + "login",
function (req, res){
  console.log("POST v3/login");
  let email = req.body.email;
  let pass = req.body.password;
  let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
  let limFilter = 'l=1&';
  let clienteMlab = request_json.createClient(URL_mLab);
  clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
  function(error, respuestaMLab, body) {
    if(!error) {
      if (body.length == 1) { // Existe un usuario que cumple 'queryString'
        if(body[0].logged==true){
          console.log("usuario logueado con anterioridad")
          res.send({'msg':'Usuario Logueado anterioirmente'});
          }
          else{
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikeyMLab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
              res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id_user, 'name':body[0].first_name});
              // If bodyPut.n == 1, put de mLab correcto
              });
              }
              }
              else {
                res.status(404).send({"msg":"Usuario no existe."});
                }
                }
                else {
                  res.status(500).send({"msg": "Error en petición a mLab."});
                  }
  });
});

/***************************
logout con ID
***************************/
app.post(URL_BASE + "logout/:v_id",
function (req, res){
  console.log("POST /v3/logout x id");
  Vid=req.params.v_id;
  let queryString = 'q={"id_user":' + Vid + '}&';
  //let limFilter = 'l=1&';
  let clienteMlab = request_json.createClient(URL_mLab);
  console.log(URL_mLab + 'user?' +queryString +apikeyMLab);
  clienteMlab.get('user?'+ queryString + apikeyMLab,
    function(error, respuestaMLab, body) {
      if(!error) {
        //console.log("prueba01")
        if (body.length >= 1 && body[0].logged==true) { // Existe un usuario que cumple 'queryString'
          console.log("prueba02"+ "----------"+ body.length)
          if(body[0].logged==true){
            var Vsession={"logged":true};
            var Vlogout = '{"$unset":' + JSON.stringify(Vsession) + '}';
            console.log("Entranfo al Logout")
            clienteMlab.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikeyMLab, JSON.parse(Vlogout),
            function(errPut, resPut, bodyPut) {
              res.send(body[0]);
              console.log("Logut Exitoso")
            });
          }
        }
          else{
              //res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id_user, 'name':body[0].first_name});
              console.log("No se encontro logueo de usuario")
              res.status(500).send({"msg": "Error en petición a mLab."});
            }
          }
  });
});

//Method POST logout con mail
app.post(URL_BASE + "logout",
function (req, res){
  console.log("POST /v3/logout con mail ");
  var email= req.body.email;
  var queryStringEmail='q={"email":"' + email + '"}&';
  var  clienteMlab = request_json.createClient(URL_mLab);
  clienteMlab.get('user?'+ queryStringEmail+apikeyMLab ,
  function(error, respuestaMLab , body) {
    console.log("entro al get");
    var respuesta = body[0];
    console.log(respuesta);
    if(respuesta!=undefined){
          console.log("logout Correcto");
          var session={"logged":true};
          var logout = '{"$unset":' + JSON.stringify(session) + '}';
          //console.log(logout + '-------'+ JSON.stringify(session) + '-------'+ "id_user  " + respuesta.first_name);
          clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikeyMLab, JSON.parse(logout),
           function(errorP, respuestaMLabP, bodyP) {
            res.send(body[0]);
            //res.send({"msg": "logout Exitoso"});
          });
    }else{
      console.log("Error en logout");
      res.send({"msg": "Error en logout"});
    }
  });
});
